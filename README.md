##Overview
python script  
とりあえずソフ演用

##Description
system_test.py:  
ファイルを1つ引数にとるプログラムの出力結果をテキストに保存  
フォルダを引数に与えた場合はフォルダ内のファイルそれぞれ対してプログラムを実行  
ほぼ大丈夫たぶん

file_structure.py:  
ファイルのヘッダーを再帰的に調べツリー状に表示  
まあまあ大丈夫

##Requirement
python3

##Usage
-h  

example:  
~/workspace/software5/src$ system_test.py sortware5 mppl/  
~$ system_test.py ~/workspace/software5/src/sortware5 ~/Document/test_data.mpl ~/Document/kekka  
~/workspace/software5/src$ file_structre.py token-list.c -I ~/  

##Install
$ git clone https://zhrbkysk@bitbucket.org/zhrbkysk/.python_script.git  
$ which env  
で/usr/bin/envを確認

$ echo $PATH  
で確認してパスの通っているところにbinの中身を置く、utilsも

もしくはパス通す(こっちのがいいかも)  
homeにディレクトリ置いて~/.bashrc最後にexport PATH=~/python_script/bin:$PATH追加  
ログアウトイン
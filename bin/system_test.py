#!/usr/bin/env python3

import argparse
from os import getcwd, listdir, mkdir, rmdir, remove, walk
from os.path import isfile, isdir, join, abspath, dirname, basename
from subprocess import getoutput

import sys
sys.path.append(join(dirname(abspath(__file__)), ".."))
from utils.utility import print_error


parser = argparse.ArgumentParser(description="description:\n  Perform system test.",
                                 usage="\n  system_test.py exec_file test_data [res_dir]",
                                 formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("exec_file", help=":an executable file that takes one argument")
parser.add_argument("test_data", help=":a test data file or a directory including test data files")
parser.add_argument("result_directory", metavar="res_dir",
                    help=":a directory for result files. (default:result)", nargs="?", default="result")
parser.add_argument("-S", action="store_true", help=":show contents of test data file")
args = vars(parser.parse_args())
exec_file = args["exec_file"]
test_data = args["test_data"]
result_dir = args["result_directory"]
f_show_test = args["S"]

if not isfile(exec_file):
    print_error("exec_file(\"" + exec_file + "\") does not exist!")
    exit()

if isdir(test_data):
    test_data_dir = test_data.rstrip("/")
    files = listdir(test_data)
elif isfile(test_data):
    test_data_dir = dirname(test_data)
    files = [basename(test_data)]
else:
    print_error("test_data(\"" + test_data + "\") does not exist!")
    exit()
files.sort()

if not isdir(result_dir):
    mkdir(result_dir)


print("------------------------------------------------------------")
print("current directory: " + getcwd())
print("test date files: " + test_data_dir + "/")
for f in files:
    print("    " + f)
print("------------------------------------------------------------")

exe = join(dirname(exec_file), "./" + basename(exec_file))
for f in files:
    cmd = exe + " " + join(test_data_dir, f)
    result_file = join(result_dir, str(f.split(".")[0]) + "_result" + ".txt")

    print("\n\n------------------------------------------------------------")
    print("command: " + cmd)
    print("result: " + result_file)
    print("------------------------------------------------------------\n")

    if f_show_test:
        print("-----------------------test data file-----------------------")
        with open(join(test_data_dir, f)) as f_:
            print(f_.read())
        print("")

    result = getoutput(cmd)
    print("---------------------------result---------------------------")
    print(result + "\n")

    with open(result_file, "w") as f_:
        f_.write(result)

print("\n\nfinish and save result in " + join(getcwd(), result_dir) + "/")
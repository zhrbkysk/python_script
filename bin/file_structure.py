#!/usr/bin/env python3

import argparse
from os import walk, getcwd
from os.path import join, abspath, dirname, basename
from re import split

import sys
sys.path.append(dirname(abspath(__file__)) + "/..")
from utils.utility import print_warn


parser = argparse.ArgumentParser(description="description:\n  Check file structure.",
                                 usage="\n  file_structure.py main_file [-I <search_path>...]",
                                 formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("main_file", type=argparse.FileType("r"), help=":a top-level-file to begin searching")
parser.add_argument("-I", metavar="", nargs="+", default=[], help=":add search paths")
parser.add_argument("search_path", nargs="*", help=":search this path for header (default:including main_file)")
main_file = parser.parse_args().main_file

if parser.parse_args().search_path:
    parser.print_usage()
    exit()


def find_file():
    if not hasattr(find_file, "walk_ob"):
        search_path = parser.parse_args().I
        search_path.append(join(getcwd(), dirname(main_file.name)))
        find_file.walk_ob = list(map(walk, search_path))

    for w in find_file.walk_ob:
        for root, dirs, files in w:
            for file in files:
                yield join(root, file)


def search_include(file):
    if not hasattr(search_include, "searched_header"):
        search_include.searched_header = set()
        search_include.find_header = set()
        search_include.nest = 0
        print(basename(file.name))

    if isinstance(file, str):
        file = open(file, "r")
    search_include.nest = search_include.nest + 1
    search_include.searched_header.add(file.name)
    try:
        for line in file:
            if line and line[:8] == "#include":
                header = split("[<>\"]", line)[1]
                print("  " * search_include.nest + basename(header))

                def match_header(h):
                    return header == "/".join(h.split("/")[-header.count("/") - 1:])
                find = list(filter(match_header, search_include.find_header))

                if find:
                    if find[0] not in search_include.searched_header:
                        search_include(find[0])
                else:
                    for f in find_file():
                        if basename(f)[-1:] == "h":
                            search_include.find_header.add(f)
                        if f[-len(header):] == header:
                            if f not in search_include.searched_header:
                                search_include(f)
    except UnicodeDecodeError:
        print_warn("UnicodeDecodeError, " + file.name)
    except IndexError:
        print_warn("IndexError, #include ???")

    file.close()
    search_include.nest = search_include.nest - 1


search_include(main_file)
#! /usr/bin/env python3

file = open("sample11pp.csl", "r")
library = open("general_library.txt", "w")

start = False
for line in file:
    line = line[:-1]
    if line == "EOVF":
        start = True
    if start and line[0] != ";":
        line = line.split(";")[0]
        line = line.replace("  ", "\t")
        library.write("\"" + line + "\\n\"\n")
file.close()
library.close()

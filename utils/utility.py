def print_error(*msg):
    print("\033[31m[error]: " + "\n         ".join(msg) + "\033[0m")


def print_warn(*msg):
    print("\033[33m[warn]: " + "\n         ".join(msg) + "\033[0m")